class Card < ApplicationRecord
  include RankedModel

  belongs_to :stage

  ranks :row_order, with_same: :stage_id

  validates :name, presence: true, length: { in: 1..140 }
end
