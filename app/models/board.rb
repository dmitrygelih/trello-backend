class Board < ApplicationRecord
  has_many :stages
  has_many :cards, through: :stages

  validates :name, presence: true, uniqueness: true, length: { in: 1..30 }
end
