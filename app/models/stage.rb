class Stage < ApplicationRecord
  belongs_to :board
  has_many :cards, dependent: :destroy

  validates :name, presence: true, length: { in: 1..30 }
end
