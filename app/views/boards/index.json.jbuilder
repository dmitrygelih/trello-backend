# frozen_string_literal: true

json.array! @boards, :id, :name
