json.set! :stages do
  @board.stages.each do |stage|
    json.set! stage.id.to_s do
      json.(stage, :id, :name)
      json.cardIds stage.cards.sort_by(&:row_order).pluck(:id)
    end
  end
end

json.set! :cards do
  @board.cards.sort_by(&:row_order).each do |card|
    # todo: n+1
    json.set! card.id.to_s do
      json.partial! 'cards/card', card: card
    end
  end
end
