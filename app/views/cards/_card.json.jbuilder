json.(card, :name, :description)
json.id card.id.to_s
json.stageId card.stage_id
json.dueDate card.due_date