json.set! :card do
  json.partial! 'cards/card', card: @card
end
