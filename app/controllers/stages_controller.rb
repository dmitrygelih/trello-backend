# frozen_string_literal: true

class StagesController < ApplicationController
  def create
    stage = Stage.create!(stage_params)
    render json: { stage: stage, message: 'Stage successfully created' }, status: :created
  end

  def destroy
    Stage.destroy(params.require(:id))
    render json: { message: 'Stage successfully deleted' }, status: :no_content
  end

  def update
    stage = Stage.find(params.require(:id))
    stage.update(stage_params)
    render json: { stage: stage, message: 'Stage successfully updated' }, status: :ok
  end

  private

  def stage_params
    params.require(:stage).permit(:name, :board_id)
  end
end
