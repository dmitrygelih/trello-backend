# frozen_string_literal: true

class BoardsController < ApplicationController
  def index
    @boards = Board.all
  end

  def create
    board = Board.create!(board_params)
    render json: { board: board, message: 'Successfully created' }, status: :created
  end

  def show
    @board = Board.includes(stages: [:cards]).find(params.require(:id))
  end

  private

  def board_params
    params.require(:board).permit(:name)
  end
end
