# frozen_string_literal: true

class CardsController < ApplicationController
  def create
    @card = Card.create!(card_params)
  end

  def update
    @card = Card.find(params.require(:id))
    @card.update!(card_params)
  end

  def update_position
    Card.update(params.require(:id), { row_order_position: params.require(:position), stage_id: params.require(:stage_id) })
    render json: { message: 'Card successfully updated' }, status: 200
  end

  private

  def card_params
    params.require(:card).permit(:name, :stage_id, :description, :due_date)
  end
end
