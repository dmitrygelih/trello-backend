Rails.application.routes.draw do
  scope '/api', defaults: {format: :json} do
    resources :boards, only: [:create, :show, :index]
    resources :stages, only: [:create, :destroy, :update]
    resources :cards, only: [:create, :update] do
      put 'update_position', to: 'cards#update_position'
    end
  end
end
