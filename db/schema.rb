# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_08_25_101022) do

  create_table "boards", force: :cascade do |t|
    t.string "name", null: false
  end

  create_table "cards", force: :cascade do |t|
    t.string "name", null: false
    t.text "description"
    t.integer "row_order", null: false
    t.datetime "due_date"
    t.integer "stage_id"
    t.index ["stage_id"], name: "index_cards_on_stage_id"
  end

  create_table "stages", force: :cascade do |t|
    t.string "name", null: false
    t.integer "board_id"
    t.index ["board_id"], name: "index_stages_on_board_id"
  end

  add_foreign_key "cards", "stages"
  add_foreign_key "stages", "boards"
end
