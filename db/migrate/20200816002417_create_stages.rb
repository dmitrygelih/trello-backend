class CreateStages < ActiveRecord::Migration[6.0]
  def change
    create_table :stages do |t|
      t.string :name, null: false, in: 1..60
      t.references :board, type: :integer, foreign_key: true, index: true
    end
  end
end
