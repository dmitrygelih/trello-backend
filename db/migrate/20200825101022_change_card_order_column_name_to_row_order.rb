class ChangeCardOrderColumnNameToRowOrder < ActiveRecord::Migration[6.0]
  def change
    rename_column :cards, :order, :row_order
  end
end
