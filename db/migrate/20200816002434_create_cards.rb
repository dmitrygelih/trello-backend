class CreateCards < ActiveRecord::Migration[6.0]
  def change
    create_table :cards do |t|
      t.string :name, null: false, in: 1..140
      t.text :description
      t.integer :order, null: false
      t.datetime :due_date
      t.references :stage, type: :integer, foreign_key: true, index: true
    end
  end
end
